package com.chhen.homework05movieapp.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chhen.homework05movieapp.R;
import com.chhen.homework05movieapp.adapters.ParentViewAdapter;
import com.chhen.homework05movieapp.models.ParentModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<ParentModel> parentModelArrayList = new ArrayList<>();
    private RecyclerView parentRecyclerView;
    private RecyclerView.Adapter ParentAdapter;
    private RecyclerView.LayoutManager parentLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        parentModelArrayList.add(new ParentModel("Trending"));
        parentModelArrayList.add(new ParentModel("New Movies"));
        parentModelArrayList.add(new ParentModel("May You Like"));
        parentModelArrayList.add(new ParentModel("TV Series"));

        parentRecyclerView = findViewById(R.id.parent_recyclerView);
        parentRecyclerView.setHasFixedSize(true);
        parentLayoutManager = new LinearLayoutManager(this);
        ParentAdapter = new ParentViewAdapter(MainActivity.this, parentModelArrayList);
        parentRecyclerView.setLayoutManager(parentLayoutManager);
        parentRecyclerView.setAdapter(ParentAdapter);
        ParentAdapter.notifyDataSetChanged();

    }
}