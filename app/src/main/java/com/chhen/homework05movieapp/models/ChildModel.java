package com.chhen.homework05movieapp.models;

public class ChildModel {

    private int imgView;
    private String movieName;

    public ChildModel(int imgView, String movieName) {
        this.imgView = imgView;
        this.movieName = movieName;
    }

    public int getImgView() {
        return imgView;
    }

    public void setImgView(int imgView) {
        this.imgView = imgView;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}
