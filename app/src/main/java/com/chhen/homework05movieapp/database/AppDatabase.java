package com.chhen.homework05movieapp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Category.class, Movie.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public static final String DB_NAME = "app_db";

    private static AppDatabase mInstance;

    public static AppDatabase getInstance(Context context){
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context, AppDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return mInstance;
    }

    public abstract CategoryDao categoryDao();
    public abstract MovieDao movieDao();

}
