package com.chhen.homework05movieapp.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface CategoryDao {

    @Insert
    void insert(Category category);

    @Transaction
    @Query("SELECT * FROM category_table")
    List<CategoryWithMovie> getCategoryWithMovies();

    @Transaction
    @Query("SELECT * FROM category_table WHERE categoryId = :categoryId")
    List<CategoryWithMovie> getMovieByCategory(long categoryId);

}
