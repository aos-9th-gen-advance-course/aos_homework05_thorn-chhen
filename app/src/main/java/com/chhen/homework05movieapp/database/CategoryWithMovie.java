package com.chhen.homework05movieapp.database;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class CategoryWithMovie {

    @Embedded
    public Category category;

    @Relation(
            parentColumn = "categoryId",
            entityColumn = "movieId"
    )
    public List<Movie> movies;

}
