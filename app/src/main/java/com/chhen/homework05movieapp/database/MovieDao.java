package com.chhen.homework05movieapp.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

@Dao
public interface MovieDao {

    @Insert
    void insert(Movie movie);

    @Transaction
    @Query("SELECT * FROM movie_table")
    List<Movie> getMovies();

    @Transaction
    @Query("SELECT * FROM category_table WHERE categoryId = :categoryId")
    List<CategoryWithMovie> getMovieByCategory(long categoryId);

}
