package com.chhen.homework05movieapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chhen.homework05movieapp.R;
import com.chhen.homework05movieapp.models.ChildModel;
import com.chhen.homework05movieapp.models.ParentModel;

import java.util.ArrayList;

public class ParentViewAdapter extends RecyclerView.Adapter<ParentViewAdapter.MyViewHolder> {

    private final ArrayList<ParentModel> parentModelArrayList;
    public Context context;

    public ParentViewAdapter(Context context, ArrayList<ParentModel> parentModelArrayList) {
        this.context = context;
        this.parentModelArrayList = parentModelArrayList;
    }

    @NonNull
    @Override
    public ParentViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.parent_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ParentViewAdapter.MyViewHolder holder, int position) {
        ParentModel currentItem = parentModelArrayList.get(position);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.childRecyclerView.setLayoutManager(layoutManager);
        holder.childRecyclerView.setHasFixedSize(true);


        holder.category.setText(currentItem.movieCategory());
        ArrayList<ChildModel> arrayList = new ArrayList<>();

        if (parentModelArrayList.get(position).movieCategory().equals("Trending")) {
            arrayList.add(new ChildModel(R.drawable.avenger1, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger2, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger3, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger4, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger5, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger6, "Movie Name"));
        }
        if (parentModelArrayList.get(position).movieCategory().equals("New Movies")) {
            arrayList.add(new ChildModel(R.drawable.avenger1, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger2, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger3, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger4, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger5, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger6, "Movie Name"));
        }
        if (parentModelArrayList.get(position).movieCategory().equals("May You Like")) {
            arrayList.add(new ChildModel(R.drawable.avenger1, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger2, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger3, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger4, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger5, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger6, "Movie Name"));
        }
        if (parentModelArrayList.get(position).movieCategory().equals("TV Series")) {
            arrayList.add(new ChildModel(R.drawable.avenger1, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger2, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger3, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger4, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger5, "Movie Name"));
            arrayList.add(new ChildModel(R.drawable.avenger6, "Movie Name"));
        }

        ChildViewAdapter childViewAdapter = new ChildViewAdapter(arrayList, holder.childRecyclerView.getContext());
        holder.childRecyclerView.setAdapter(childViewAdapter);
    }


    @Override
    public int getItemCount() {
        return parentModelArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView category;
        public RecyclerView childRecyclerView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            category = itemView.findViewById(R.id.movie_category);
            childRecyclerView = itemView.findViewById(R.id.child_rv);
        }
    }
}
