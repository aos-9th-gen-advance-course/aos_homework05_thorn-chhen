package com.chhen.homework05movieapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chhen.homework05movieapp.models.ChildModel;
import com.chhen.homework05movieapp.R;

import java.util.ArrayList;

public class ChildViewAdapter extends RecyclerView.Adapter<ChildViewAdapter.MyViewHolder> {

    public ArrayList<ChildModel> childModelArrayList;
    Context context;

    public ChildViewAdapter(ArrayList<ChildModel> childModelArrayList, Context context) {
        this.childModelArrayList = childModelArrayList;
        this.context = context;
    }

    @NonNull
    @Override
    public ChildViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildViewAdapter.MyViewHolder holder, int position) {
        ChildModel currentItem = childModelArrayList.get(position);
        holder.imgView.setImageResource(currentItem.getImgView());
        holder.movieName.setText(currentItem.getMovieName());
    }

    @Override
    public int getItemCount() {
        return childModelArrayList.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgView;
        public TextView movieName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgView = itemView.findViewById(R.id.img_view);
            movieName = itemView.findViewById(R.id.movie_name);
        }
    }
}
